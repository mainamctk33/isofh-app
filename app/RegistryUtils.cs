﻿using Microsoft.Win32;
using System;

namespace SharpBrowser
{
    class RegistryUtils
    {
        public static RegistryKey OpenKey()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software", true);
            var temp = key.OpenSubKey("iSofH", true);
            if (temp != null)
                return temp;
            return key.CreateSubKey("iSofH", RegistryKeyPermissionCheck.ReadWriteSubTree);
        }
        public static RegistryKey OpenKey(String keyString)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software", true);
            var temp = key.OpenSubKey(keyString, true);
            if (temp != null)
                return temp;
            return key.CreateSubKey(keyString, RegistryKeyPermissionCheck.ReadWriteSubTree);
        }
        public static RegistryKey OpenKey(RegistryKey registry, String keyString)
        {
            var temp = registry.OpenSubKey(keyString, true);
            if (temp != null)
                return temp;
            return registry.CreateSubKey(keyString, RegistryKeyPermissionCheck.ReadWriteSubTree);
        }
        public static void SetValue(String key, object value)
        {
            var temp = OpenKey();
            temp.SetValue(key, value);
        }
        public static int GetValue(String key, int defaultValue)
        {
            try
            {
                var temp = OpenKey();
                if (temp != null)
                {
                    var value = temp.GetValue(key);
                    return Convert.ToInt16(value);                    
                }
            }
            catch (Exception)
            {

            }
            return defaultValue;
        }
        public static int GetValue(String key)
        {
            return GetValue(key, 0);
        }

        public static string GetStringValue(String key)
        {
            try
            {
                var temp = OpenKey();
                if (temp != null)
                {
                    var value = temp.GetValue(key);
                    return value.ToString();
                }
            }
            catch (Exception)
            {

            }
            return "";
        }

    }
}
