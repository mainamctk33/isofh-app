﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Timer = System.Windows.Forms.Timer;
using System.Drawing;
using System.Reflection;
using CefSharp;
using CefSharp.WinForms;

namespace SharpBrowser
{

    /// <summary>
    /// The main SharpBrowser form, supporting multiple tabs.
    /// We used the x86 version of CefSharp V51, so the app works on 32-bit and 64-bit machines.
    /// If you would only like to support 64-bit machines, simply change the DLL references.
    /// </summary>
    /// 
    internal partial class frmWebView : Form
    {
        object browser;
        String url = "";
        int screen;

        public frmWebView(String url, String quay, int screen)
        {
            String time = RegistryUtils.GetStringValue("TIME");
            String speaker = RegistryUtils.GetStringValue("SPEAKER");
            String type = RegistryUtils.GetStringValue("TYPE_VALUE");
            this.url = url + "medicaltest/" + quay + $"?timer={time}&speaker={speaker}&type={type}";


            InitializeComponent();
            // set config
            if (RegistryUtils.GetValue("IEMODE") == 1)
            {
                browser = new WebBrowser();
                ((WebBrowser)browser).Url = new Uri(this.url);
                ((WebBrowser)browser).Dock = DockStyle.Fill;
                ((WebBrowser)browser).ScriptErrorsSuppressed = true;
                ((WebBrowser)browser).IsWebBrowserContextMenuEnabled= false;                
            }
            else
            {
                browser = new CefSharp.WinForms.ChromiumWebBrowser(this.url);
                //validate if the CEF instance was already initialized
                CefSharp.CefSettings settings = new CefSharp.CefSettings()
                {
                    CachePath = "Cache"
                };
                settings.IgnoreCertificateErrors = true;

                if (CefSharp.Cef.IsInitialized == false)
                    CefSharp.Cef.Initialize(settings, true, true);
                // set layout
                ((CefSharp.WinForms.ChromiumWebBrowser)browser).Dock = DockStyle.Fill;
                ((CefSharp.WinForms.ChromiumWebBrowser)browser).BringToFront();
                ((CefSharp.WinForms.ChromiumWebBrowser)browser).ContextMenuStrip = null;
                ((CefSharp.WinForms.ChromiumWebBrowser)browser).MenuHandler = null;
            }
            this.Controls.Add(((Control)browser));

            this.screen = screen;
            if (screen >= Screen.AllScreens.Length)
            {
                this.screen = 0;
            }
            Screen _screen = Screen.AllScreens[this.screen];
            this.Location = _screen.Bounds.Location;
            this.MinimumSize = _screen.WorkingArea.Size;
            //WindowState = FormWindowState.Maximized;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
        }


        private void timer2_Tick(object sender, EventArgs e)
        {
            if (browser != null)
            {
                if (browser is CefSharp.WinForms.ChromiumWebBrowser)
                {
                    ((CefSharp.WinForms.ChromiumWebBrowser)browser).ExecuteScriptAsync("load()");
                }
                else
                {
                    ((WebBrowser)browser).Document.InvokeScript("load");
                }
            }
        }

        public void reload()
        {
            timer2_Tick(null, null);
        }
        public bool isClose = false;

        private void frmWebView_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isClose)
                e.Cancel = true;
        }

        private void frmWebView_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            this.Top = Screen.AllScreens[screen].Bounds.Top;
            this.Left = Screen.AllScreens[screen].Bounds.Left;
            this.MinimumSize = Screen.AllScreens[screen].Bounds.Size;
            WindowState = FormWindowState.Maximized;
        }

        private void frmWebView_SizeChanged(object sender, EventArgs e)
        {
            if (this.browser != null)
            {
                ((Control)browser).Size = this.Size;
                ((Control)browser).Top = 0;
                ((Control)browser).Left = 0;
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                var Params = base.CreateParams;
                Params.ExStyle |= 0x80;
                return Params;
            }
        }
    }
}