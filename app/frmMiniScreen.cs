﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Net;
using System.Threading;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using SharpBrowser.Models;
using System.IO;

namespace SharpBrowser
{
    public partial class frmMiniScreen : Form
    {
        int screen2;
        frmWebView webView;
        String quay;
        bool start = false;
        String webUrl;
        String hisUrl;
        List<DataEntity> listWait = new List<DataEntity>();
        List<DataEntity> listDisplayed = new List<DataEntity>();
        List<String> listCodeDisplayed = new List<string>();

        public frmMiniScreen(String hisUrl, String webUrl, int screen, int screen2, String quay)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            this.hisUrl = hisUrl;
            if (!this.hisUrl.EndsWith("/"))
                this.hisUrl += "/";
            this.hisUrl += "isofh/services/imgdiag/getPatientByRoom/" + quay;
            this.webUrl = webUrl;
            if (!this.webUrl.EndsWith("/"))
                this.webUrl += "/";

            if (!this.hisUrl.ToLower().StartsWith("http"))
                this.hisUrl = "http://" + this.hisUrl;

            if (!this.webUrl.ToLower().StartsWith("http"))
                this.webUrl = "http://" + this.webUrl;

            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.Fixed3D;
            this.TopMost = true;
            this.WindowState = FormWindowState.Normal;
            this.StartPosition = FormStartPosition.CenterScreen;


            this.quay = quay;
            this.screen2 = screen2;
            if (screen >= Screen.AllScreens.Length)
            {
                this.screen2 = 0;
            }
            Screen _screen = Screen.AllScreens[this.screen2];
            this.Location = _screen.Bounds.Location;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            lbName.Text = "";
            lbCurrentPatient.Text = "";
            webView = new frmWebView(this.webUrl, quay, screen);
            webView.Show();
            timer1_Tick(null, null);
        }

        private void frmMiniScreen_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            this.Top = Screen.AllScreens[screen2].Bounds.Top;
            this.Left = Screen.AllScreens[screen2].Bounds.Left;
            this.Text = this.Text + " v."+Application.ProductVersion;

        }
        public bool isClose = false;
        private bool hasRead;
        private DataEntity current;

        public event EventHandler onClose;
        private void frmMiniScreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isClose)
            {
                e.Cancel = true;
                if (MessageBox.Show("Bạn có muốn thoát ứng dụng gọi số? Các màn hình sẽ không còn hoạt động nếu nhấn OK", "Xác nhận",MessageBoxButtons.OKCancel)==DialogResult.OK)
                {
                    isClose = true;
                    if (onClose != null)
                        onClose(sender, e);
                }
            }
            else
            {
                if (webView != null)
                {
                    webView.isClose = true;
                    webView.Close();
                }
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (listDisplayed.Count == 0)
            {
                MessageBox.Show("Chưa có bệnh nhân nào được gọi");
                return;
            }
            DataEntity data = listDisplayed.First();
            if (PostToScreen(data))
            {
                listWait.Insert(0, data);
                listDisplayed.Remove(data);
                listCodeDisplayed.Remove(data.code);
                current = data;
                showDataScreen();
                webView.reload();
                saveFile();
            }
            else
            {
                MessageBox.Show("Cập nhật không thành công");
            }
        }

        bool PostToScreen(DataEntity data)
        {
            String url = webUrl + "api/wait/" + quay;
            using (WebClient client = new WebClient())
            {
                try
                {
                    WebHeaderCollection headers = new WebHeaderCollection();
                    client.Headers = headers;
                    headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Encoding = Encoding.UTF8;
                    if (data.textToAudio == null)
                        data.textToAudio = "";
                    var method = "POST"; // If your endpoint expects a GET then do it.
                    String content = new JavaScriptSerializer().Serialize(new List<DataEntity>() { data });
                    var arr = data.code.Split('.');
                    content = content.Replace(data.code, arr[arr.Length - 1]);
                    var response_data = client.UploadString(new Uri(url), method, content);

                    JavaScriptSerializer javascript = new JavaScriptSerializer();
                    dynamic s3 = javascript.Deserialize<dynamic>(response_data);
                    if (s3.Count != 0)
                    {
                        dynamic s4 = s3["code"];
                        if (s4 == "200")
                            return true;
                    }
                    return false;
                }
                catch (Exception ex)
                {
                }
            }
            return false;
        }

        void readFile()
        {
            try
            {
                if (hasRead)
                    return;
                String text = File.ReadAllText(DateTime.Now.ToString("ddMMyyyy") + ".dat");
                var dataSave = new JavaScriptSerializer().Deserialize<DataSave>(text);
                if (!(hisUrl == dataSave.hisUrl && webUrl == dataSave.webUrl && quay == dataSave.quay))
                {
                    hasRead = true;
                    return;
                }
                current = dataSave.current;
                listCodeDisplayed = dataSave.listCodeDisplayed;

                foreach (var code in listCodeDisplayed)
                {
                    var item = listWait.Find(x => x.code == code);
                    if (item != null)
                    {
                        listWait.Remove(item);
                        listDisplayed.Add(item);
                    }
                }
                hasRead = true;
            }
            catch (Exception e)
            {
                hasRead = true;
            }
        }
        public class DataSave
        {
            public String quay { get; set; }
            public String hisUrl { get; set; }
            public String webUrl { get; set; }
            public DataEntity current { get; set; }
            public List<String> listCodeDisplayed { get; set; }
        }

        void saveFile()
        {
            try
            {
                string createText = new JavaScriptSerializer().Serialize(new DataSave() { current = current, listCodeDisplayed = listCodeDisplayed, webUrl = webUrl, hisUrl = hisUrl, quay = quay });

                File.WriteAllText(DateTime.Now.ToString("ddMMyyyy") + ".dat", createText);
            }
            catch (Exception)
            {
            }
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (listWait.Count == 0)
            {
                MessageBox.Show("Danh sách chờ đang rỗng");
                return;
            }
            DataEntity data = listWait.First();
            if (PostToScreen(data))
            {
                listDisplayed.Insert(0, data);
                listCodeDisplayed.Insert(0, data.code);
                listWait.Remove(data);
                current = data;
                saveFile();
                showDataScreen();
                webView.reload();
            }
            else
            {
                MessageBox.Show("Cập nhật không thành công");
            }
        }

        private void showDataScreen()
        {

            if (current != null)
            {
                var arr = current.code.Split('.');
                lbCurrentPatient.Text = arr[arr.Length - 1];

                lbName.Text = current.name + " " + current.age + "T";
            }
            else
            {
                lbCurrentPatient.Text = "";
                lbName.Text = "";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    client.Encoding = Encoding.UTF8;
                    string s = client.DownloadString(hisUrl);
                    Console.WriteLine(s);
                    JavaScriptSerializer javascript = new JavaScriptSerializer();
                    dynamic s3 = javascript.Deserialize<dynamic>(s);
                    if (s3.Count != 0)
                    {
                        dynamic s4 = s3["data"];
                        for (var i = 0; i < s4.Length; i++)
                        {
                            var s5 = s4[i];
                            String code = s5["code"];
                            var item = listWait.Find(x => x.code == code);
                            if (item == null)
                            {
                                if (listDisplayed.Find(x => x.code == code) != null)
                                    continue;
                                String json = javascript.Serialize(s5);
                                var temp = javascript.Deserialize<DataEntity>(json);
                                listWait.Add(temp);
                            }
                        }
                    }
                    listWait = listWait.OrderByDescending(x => x.priority).ThenBy(x => x.numCode).ToList();
                    readFile();
                    showDataScreen();
                    if (current == null)
                    {
                        if (listWait.Count > 0)
                            btnNext.PerformClick();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }
    }
}
