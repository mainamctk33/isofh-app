﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SharpBrowser
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new frmTest());
            if (args.Length > 0)
            {
                Application.Run(new frmMain(Convert.ToBoolean(args[0])));
            }
            else
            {
                try
                {
                    Application.Run(new frmMain(false));
                }
                catch (Exception)
                {

                }
            }
        }
    }
}
