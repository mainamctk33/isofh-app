﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace SharpBrowser
{
    public partial class frmSetting : Form
    {
        public static bool show = false;
        public frmSetting()
        {
            show = true;
            InitializeComponent();
            txtWebUrl.Text = RegistryUtils.GetStringValue("WEB_URL");
            txtQuay.Text = RegistryUtils.GetStringValue("QUAY");
            txtHisURL.Text = RegistryUtils.GetStringValue("HIS_URL");

            cbTime.Text = RegistryUtils.GetStringValue("TIME");
            cbType.SelectedIndex = RegistryUtils.GetValue("TYPE", 0);
            cbSpeaker.SelectedIndex = RegistryUtils.GetValue("SPEAKER", 0);
            cbIE.Checked = RegistryUtils.GetValue("IEMODE", 0) == 1;

            var screens = Screen.AllScreens;
            for (int i = 0; i < screens.Count(); i++)
            {
                cbScreen.Items.Add(i + 1);
            }
            cbScreen.Text = RegistryUtils.GetStringValue("SCREEN");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                RegistryUtils.SetValue("WEB_URL", txtWebUrl.Text);
                RegistryUtils.SetValue("SCREEN", cbScreen.Text);
                RegistryUtils.SetValue("QUAY", txtQuay.Text);
                RegistryUtils.SetValue("HIS_URL", txtHisURL.Text);
                RegistryUtils.SetValue("TIME", cbTime.Text);
                RegistryUtils.SetValue("TYPE_VALUE", cbType.SelectedIndex == 0 ? "s" : cbType.SelectedIndex == 1 ? "c" : "r");
                RegistryUtils.SetValue("TYPE", cbType.SelectedIndex);
                RegistryUtils.SetValue("SPEAKER", cbSpeaker.SelectedIndex);
                RegistryUtils.SetValue("IEMODE", cbIE.Checked ? 1 : 0);

                RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                if (chkRunStartup.Checked)
                    rkApp.SetValue("iSofH", Application.ExecutablePath.ToString());
                else
                    rkApp.SetValue("iSofH", "");
                DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Xảy ra lỗi, vui lòng thử lại " + ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmSetting_Load(object sender, EventArgs e)
        {
            try
            {
                RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                var value = rkApp.GetValue("iSofH").ToString();
                var run = value != null && !String.IsNullOrWhiteSpace(value);
                chkRunStartup.Checked = run;
            }
            catch (Exception ex)
            {

            }
        }

        private void frmSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            show = false;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtHISUrl_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
