﻿namespace SharpBrowser
{
    partial class frmSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetting));
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtWebUrl = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbScreen = new System.Windows.Forms.ComboBox();
            this.chkRunStartup = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtQuay = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtHisURL = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbTime = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbSpeaker = new System.Windows.Forms.ComboBox();
            this.cbIE = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(477, 232);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(76, 24);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Lưu";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(395, 232);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(76, 24);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Hủy";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Server web gọi số (vd: http://10.0.0.86:9091)";
            // 
            // txtWebUrl
            // 
            this.txtWebUrl.Location = new System.Drawing.Point(8, 118);
            this.txtWebUrl.Name = "txtWebUrl";
            this.txtWebUrl.Size = new System.Drawing.Size(260, 20);
            this.txtWebUrl.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Hiển thị trên màn hình";
            // 
            // cbScreen
            // 
            this.cbScreen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbScreen.FormattingEnabled = true;
            this.cbScreen.Location = new System.Drawing.Point(8, 162);
            this.cbScreen.Name = "cbScreen";
            this.cbScreen.Size = new System.Drawing.Size(260, 21);
            this.cbScreen.TabIndex = 3;
            // 
            // chkRunStartup
            // 
            this.chkRunStartup.AutoSize = true;
            this.chkRunStartup.Location = new System.Drawing.Point(294, 166);
            this.chkRunStartup.Name = "chkRunStartup";
            this.chkRunStartup.Size = new System.Drawing.Size(146, 17);
            this.chkRunStartup.TabIndex = 4;
            this.chkRunStartup.Text = "Khởi động cùng windows";
            this.chkRunStartup.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Quầy";
            // 
            // txtQuay
            // 
            this.txtQuay.Location = new System.Drawing.Point(8, 25);
            this.txtQuay.Name = "txtQuay";
            this.txtQuay.Size = new System.Drawing.Size(260, 20);
            this.txtQuay.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(189, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Server HIS (vd: http://10.0.0.86:8080)";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtHisURL
            // 
            this.txtHisURL.Location = new System.Drawing.Point(8, 69);
            this.txtHisURL.Name = "txtHisURL";
            this.txtHisURL.Size = new System.Drawing.Size(260, 20);
            this.txtHisURL.TabIndex = 2;
            this.txtHisURL.TextChanged += new System.EventHandler(this.txtHISUrl_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(291, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Loại";
            // 
            // cbType
            // 
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "CLS",
            "Khám",
            "Tiếp đón"});
            this.cbType.Location = new System.Drawing.Point(294, 117);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(260, 21);
            this.cbType.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(291, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Thời gian làm mới";
            // 
            // cbTime
            // 
            this.cbTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTime.FormattingEnabled = true;
            this.cbTime.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cbTime.Location = new System.Drawing.Point(294, 25);
            this.cbTime.Name = "cbTime";
            this.cbTime.Size = new System.Drawing.Size(260, 21);
            this.cbTime.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(291, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Speaker";
            // 
            // cbSpeaker
            // 
            this.cbSpeaker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSpeaker.FormattingEnabled = true;
            this.cbSpeaker.Items.AddRange(new object[] {
            "Không đọc loa",
            "Đọc người bệnh đang thực hiện",
            "Đọc người bệnh đang chờ"});
            this.cbSpeaker.Location = new System.Drawing.Point(294, 68);
            this.cbSpeaker.Name = "cbSpeaker";
            this.cbSpeaker.Size = new System.Drawing.Size(260, 21);
            this.cbSpeaker.TabIndex = 5;
            // 
            // cbIE
            // 
            this.cbIE.AutoSize = true;
            this.cbIE.Location = new System.Drawing.Point(294, 145);
            this.cbIE.Name = "cbIE";
            this.cbIE.Size = new System.Drawing.Size(131, 17);
            this.cbIE.TabIndex = 4;
            this.cbIE.Text = "Sử dụng trình duyệt IE";
            this.cbIE.UseVisualStyleBackColor = true;
            // 
            // frmSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 268);
            this.ControlBox = false;
            this.Controls.Add(this.cbSpeaker);
            this.Controls.Add(this.cbTime);
            this.Controls.Add(this.cbType);
            this.Controls.Add(this.cbIE);
            this.Controls.Add(this.chkRunStartup);
            this.Controls.Add(this.cbScreen);
            this.Controls.Add(this.txtHisURL);
            this.Controls.Add(this.txtQuay);
            this.Controls.Add(this.txtWebUrl);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cài đặt";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSetting_FormClosing);
            this.Load += new System.EventHandler(this.frmSetting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtWebUrl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbScreen;
        private System.Windows.Forms.CheckBox chkRunStartup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtQuay;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtHisURL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbSpeaker;
        private System.Windows.Forms.CheckBox cbIE;
    }
}