﻿using System.Threading;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace SharpBrowser
{
    public partial class frmMain : Form
    {
        private frmMiniScreen miniScreen;

        public frmMain(bool showFull)
        {
            InitializeComponent();
            this.Size = new Size(0, 0);
            this.Opacity = 0;
            this.ControlBox = false;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            ShowNotification("iSofH App Gọi Số đang chạy");
            openScreen(getScreen1(), getScreen2());
        }

        void ShowNotification(string Text)
        {
            notifyIcon1.BalloonTipText = Text;
            notifyIcon1.BalloonTipTitle = "Thông Báo";
            notifyIcon1.ShowBalloonTip(2000);
        }


        private void btnSetting_Click(object sender, EventArgs e)
        {
            frmSetting setting = new frmSetting();
            if (setting.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    closeScreen();
                    openScreen(getScreen1(), getScreen2());
                }
                catch (Exception ẽ)
                {
                }
            }
        }

        int getScreen1()
        {
            int screen = RegistryUtils.GetValue("SCREEN");
            screen--;            
            if (screen >= 0 && screen < Screen.AllScreens.Count())
                return screen;
            return 0;
        }
        int getScreen2()
        {
            int screen1 = getScreen1();
            for (int i = 0; i < Screen.AllScreens.Count(); i++)
                if (i != screen1)
                    return i;
            return screen1;
        }

        void closeScreen()
        {
            if (miniScreen != null)
            {
                miniScreen.isClose = true;
                miniScreen.Close();
            }
        }

        void openScreen(int screen, int screen2)
        {
            try
            {
                String hisUrl = RegistryUtils.GetStringValue("HIS_URL").Trim();
                String quay = RegistryUtils.GetStringValue("QUAY").Trim();
                String webUrl = RegistryUtils.GetStringValue("WEB_URL").Trim();
                if (!webUrl.ToLower().StartsWith("http"))
                    webUrl = "http://" + webUrl;
                if (!hisUrl.ToLower().StartsWith("http"))
                    hisUrl = "http://" + hisUrl;

                if (hisUrl == null || String.IsNullOrWhiteSpace(hisUrl) || webUrl == null || String.IsNullOrWhiteSpace(webUrl) ||                   
                    quay == null || String.IsNullOrWhiteSpace(quay))
                {
                    btnSetting_Click(null, null);
                }
                else
                {
                    miniScreen = new frmMiniScreen(hisUrl, webUrl, screen, screen2, RegistryUtils.GetStringValue("QUAY"));
                    miniScreen.onClose += btnExit_Click;
                    miniScreen.Show();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                btnSetting_Click(null, null);
            }

        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            if (miniScreen != null)
            {
                miniScreen.isClose = true;
                miniScreen.Close();
            }
            this.Close();
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            // if you still want to hide
            Hide();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                var Params = base.CreateParams;
                Params.ExStyle |= 0x80;
                return Params;
            }
        }
    }
}
