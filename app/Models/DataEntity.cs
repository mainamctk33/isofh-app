﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBrowser.Models
{
    public class DataEntity
    {
        public String code { get; set; }
        public String address { get; set; }
        public String name { get; set; }
        public bool priority { get; set; }
        public int age { get; set; }
        public String textToAudio { get; set; }

        public int numCode
        {
            get
            {
                try
                {

                    if (code == null)
                        return int.MaxValue;
                    var arr = code.Split('.');
                    if (arr.Length >= 2)
                    {
                        return Convert.ToInt32(arr[arr.Length - 1]);
                    }
                    return int.MaxValue;
                }
                catch (Exception)
                {
                    return int.MaxValue;

                }

            }
        }
    }
}
